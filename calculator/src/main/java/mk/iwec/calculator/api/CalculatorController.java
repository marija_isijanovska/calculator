package mk.iwec.calculator.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.calculator.service.ResponseServiceImpl;

@RestController
@RequestMapping("/calc")
public class CalculatorController {

	@Autowired
	private ResponseServiceImpl rs;

	@GetMapping("/add")
	Double add(@RequestParam Double a, @RequestParam Double b) {
		return rs.getAdd(a, b);
	}
	
	 @GetMapping("/sub")
		Double suby(@RequestParam Double a, @RequestParam Double b) {
			return rs.getSub(a, b);
		}
    @GetMapping("/divide")
	Double divide(@RequestParam Double a, @RequestParam Double b) {
		return rs.getDivide(a, b);
	}
    @GetMapping("/multiply")
	Double multiply(@RequestParam Double a, @RequestParam Double b) {
		return rs.getMultiply(a, b);
	}
}