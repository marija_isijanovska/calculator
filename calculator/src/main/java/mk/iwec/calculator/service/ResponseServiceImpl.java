package mk.iwec.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class ResponseServiceImpl implements ResponseService {

	@Override
	public Double getAdd(Double a, Double b) {
		return a + b;
	}

	@Override
	public Double getSub(Double a, Double b) {
		return a-b;
	}

	@Override
	public Double getMultiply(Double a, Double b) {
		return a * b;
	}

	@Override
	public Double getDivide(Double a, Double b) {
		return a / b;
	}


}
