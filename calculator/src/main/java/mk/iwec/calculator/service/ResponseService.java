package mk.iwec.calculator.service;

public interface ResponseService {
		
		Double getAdd(Double firstNumber, Double SecondNumber);
		Double getSub(Double firstNumber, Double SecondNumber);
		Double getMultiply(Double firstNumber, Double SecondNumber);
		Double getDivide(Double firstNumber, Double SecondNumber);

}
